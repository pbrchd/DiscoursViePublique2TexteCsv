#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
from bs4 import BeautifulSoup
import logging
import csv
from datetime import datetime


def ecriturefirstbibl(datecsv):
    """initialisation"""
    c = csv.writer(
        open("Data/Csv/viepublique-" + datecsv + ".csv", "a"),
        delimiter=";",
        quotechar='"',
        quoting=csv.QUOTE_ALL,
    )
    c.writerow(["id", "titre", "date", "intervenants", "motsclefs", "url"])
    return c


def ecriturebibl(fichier, titre, date, intervenants, tags, csv):
    """ecriture du fichier CSV"""
    url = "https://www.vie-publique.fr/discours/" + fichier
    csv.writerow([fichier, titre, date, intervenants, tags, url])
    return csv


def detectiontitre(element):
    """detection du titre"""
    texte = ""
    try:
        elementdiv = element.find("h1", {"class": "h3"})
        texte = elementdiv.get_text().strip()
    except:
        logging.error("erreur value %s" % (element))
    return texte

def detectiontexte(element):
    """detection du texte"""
    texte = ""
    try:
        elementdiv = element.find("span", {"class": "field--name-field-texte-integral"})
        texte = elementdiv.get_text()
    except:
        logging.error("erreur value %s" % (element))
    return texte

def detectionintervenants(element):
    """detection des intervenants"""
    texte = ""
    listeintervenants = []
    try:
        elementdiv = element.find("div", {"class": "discour--infos"})
        elementul = elementdiv.find("ul", {"class": "line-intervenant"})
        elementli = elementul.findAll("li")
        for element in elementli:
            listeintervenants.append(element.get_text())
        texte = "_".join(listeintervenants)
    except:
        logging.error("erreur value %s" % (element))
    return texte

def detectiontags(element):
    """detection des tags"""
    texte = ""
    listetags = []
    try:
        elementdiv = element.find("div", {"class": "thematicBox"})
        elementul = elementdiv.find("ul", {"class": "tags--list"})
        elementli = elementul.findAll("li")
        for element in elementli:
            listetags.append(element.get_text().strip())
        texte = "_".join(listetags)
    except:
        logging.error("erreur value %s" % (element))
    return texte

def detectiondate(element):
    """detection de la date"""
    texte = ""
    try:
        elementdiv = element.find("time", {"class": "datetime"})
        texte = elementdiv.get_text()
    except:
        logging.error("erreur value %s" % (element))
    return texte

def ecrituretexte(nom, texte):
    fichier = open("Data/Texte/" + nom + ".txt", "a")
    fichier.write(texte)
    fichier.close()

if __name__ == "__main__":

    path = "Data/Html/"
    logging.basicConfig(
        filename="./debug.log",
        level=logging.INFO,
        format="%(asctime)s %(levelname)s - %(message)s",
        datefmt="%d/%m/%Y %H:%M:%S",
    )
    dirs = os.listdir(path)
    datecsv = str(datetime.now().strftime("%d-%m-%Y-%H-%M"))
    csv = ecriturefirstbibl(datecsv)
    for fichier in dirs:
        logging.info("Ouverture du fichier %s" % (fichier))
        html_doc = open(path + "/" + fichier, "r").read()
        soup = BeautifulSoup(html_doc, from_encoding="utf-8")
        titre = detectiontitre(soup)
        date = detectiondate(soup)
        intervenants = detectionintervenants(soup)
        tags = detectiontags(soup)
        texte = detectiontexte(soup)
        ecrituretexte(fichier, texte)
        csv = ecriturebibl(fichier, titre, date, intervenants, tags, csv)
